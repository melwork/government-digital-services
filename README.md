# Government Services Manual

##Requirements for contract work

DEV PROCESS
research > discovery > alpha > beta > public beta > live

## ACCESSIBILITY

[Accessibility - Manual](https://www.gov.uk/service-manual/helping-people-to-use-your-service/making-your-service-accessible-an-introduction)

- Your service must be accessible to everyone who needs it.
- Meet level **AA of the Web Content Accessibility Guidelines (WCAG 2.1)** minimum
  [Understanding WCAG](https://www.gov.uk/service-manual/helping-people-to-use-your-service/understanding-wcag)
- Service works on **assistive technologies** - screen magnifiers, readers, speech recognition tools
- **User research** - include people with disabilities
- **Accessibility page** that explains how accessible the service is

###Budget considerations

- **Training / time to learn about accessibility** - TRAIN content designers, interaction designers and developers, product and delivery managers
- **User reseach with people with disabilities** (recruitment agency to source participants, expenses payments). Run research sessions.
- **Accessibility audit**, third party auditor on GOV.UK costs £3,000 - £7,000
- Estimate demand for **user support** + service requirements

###Allow for:

- **Discovery + alpha phase user research**: understand users’ digital skill, confidence + access, find out user needs for digital support
- **Testing for accessibility** - with disabled and older users - at beta stage
- **Code testing** - automated testing and manual testing
- **Accessibility audit** - and fix - before service moves into public beta.
- **Publishing data on accessibility** in service [Accessibility page](https://www.gov.uk/service-manual/helping-people-to-use-your-service/publishing-information-about-your-services-accessibility)
- Research, propose - **requirement to provide assisted digital support** - in person, telephone, via webchat.

---

## AGILE DELIVERY

[Agile - Manual](https://www.gov.uk/service-manual/agile-delivery)

- start small discovery, alpha
- gather requirements, plan, design, build and test - at the same time.
- research, prototype, test and learn about your users’ needs before building the real service in the beta phase
- Go live when you have enough feedback to show your service works for users

---

## DESIGN

[Design - Manual](https://www.gov.uk/service-manual/design)

###Service design approach + considerations

- **Understand service from beginning to end:**
    - Every single thing a user needs to do (user journey)
    - Front to back – users + internal processes, software, policies behind it
    - Channels – offline + digital
- Enables user to complete the thing they set out to do in the smallest number of steps
- **User journey map** (user research)
- Use **familiar design conventions** - use [design system](https://design-system.service.gov.uk/) for common patterns
- **Language (need Content Designer)** - design good questions, emails, letters
- **Service mapping** - Which service is project linked to and what does service provide for citizens?
- Context of wider user journey - how it joins up with other user journeys
- Service collaborations and understanding **service landscape**
- **Scoping transacations** - scope for addressing the user problem
- Assess **eligibility, context and routing for users**
- **Naming service**
- **Forms design**
- **Data protection, privacy** and storage of personal data - GDPR + consent
- Accessibility for **civil servants as users**

### Prototype build process

- **Alpha phase** - explore and test - explore design ideas - sketches etc
- **Build code prototypes** - to test realistic interactions that a user has with your service
    - Use [Gov.uk Prototype Kit](https://govuk-prototype-kit.herokuapp.com/docs)
    - Publish on heroku
- Implement **user research + testing** with users (including disabilities and those who need support to use)
- **Agile user research dev process** to update understanding of users, test design ideas, content and features to see if they work well for all users, understand problems users are having and how they might be resolved
    - All team members should **watch real users interacting with your service** and talking about it
    - ideally for at least **2 hours every 6 weeks**.
- **User researchers should work with the team at least 3 days a week** (throughout each development phase) + run **user research sessions** at least every 2 weeks

### User inclusivity - design and measure

- **Recruit user research participants**
- **Design for inclusion** (e.g. visual impairments, older computers, mobile data); legal duties, equality
- Set up [performance metrics](https://www.gov.uk/service-manual/measuring-success/how-to-set-performance-metrics-for-your-service) - **KPIs** (e.g. analytics, user feedback, performance, call centre monitors) and output this data visually / dashboard

###Needs to look like Gov.uk

- Use design patterns, components and styles in the [GOV.UK Design System](https://design-system.service.gov.uk/)
- Reference [GOV.UK content style guide](https://www.gov.uk/guidance/style-guide/a-to-z-of-gov-uk-style)
- PRODUCTION - use [GOV.UK Design System and GOV.UK Frontend](https://www.gov.uk/service-manual/design/making-your-service-look-like-govuk) to implement patterns and typeface


---

## TECHNOLOGY

Choosing technology, development, integration, hosting, testing, security and maintenance.

[Toolkit and digital standards](https://www.gov.uk/service-toolkit#components)

###[Technology code of practice](https://www.gov.uk/government/publications/technology-code-of-practice/technology-code-of-practice)

**Inclusivity:**

- making your technology work for as many users as possible (easy access for staff to your organisation’s network, authoring tools, project management software and HR suites)
- assurance that there is no barrier to employing people with specific access needs

**Cloud First policy**

- use public cloud solutions
- avoid lock-in by working through abstraction layers like CloudFoundry or Kubernetes or by using common multi-cloud tooling like Terraform.

**Open source policy**

- use open source where possible
- open code on Github

**Open standards**

Open Standards Principles for software interoperability, data and document formats.
Suppliers should be aware of the open standards that are compulsory
[Open standards for government](https://www.gov.uk/government/publications/open-standards-for-government)

- e.g. time and date, vCard (contact info), RESTful APIs

* Use of open standards - should be mature, well-documented, compatible

Make data and application programming interfaces (APIs) available

**Make things secure**

- assess risk-informed security needs, mitigate applicable threats
- plan how to protect against, detect and correct malicious behaviour
- make each element secure instead of your security experts adding technical countermeasures
- National Cyber Security Centre [risk management](https://www.ncsc.gov.uk/collection/risk-management-collection)

TOUCHPOINTS:

- cloud security
- cyber security
- addressing cyber attacks and fraud
- securing data and consent
- digital service security
- email security
- passwords
- phishing

**Privacy**

- [Make Privacy Integral](https://www.gov.uk/guidance/make-privacy-integral)
- GDPR + Data Protection Act 2018
- privacy by design - reduce risks of data theft

**Share and re-use**

- share and collaborate culture

**Integrate and adapt**

- Your technology should adapt to future demands and work with existing technologies, processes and infrastructure in your organisation.

**Better use of data**

- Minimise data collection and duplication
- Reuse open data
- make data open by default
- anonymize personal data
- [API technical and data standards](https://www.gov.uk/guidance/gds-api-technical-and-data-standards)

**Service standard**
[About the service standard](https://www.gov.uk/service-manual/service-standard)

1. Understand users and their needs
2. Solve a whole problem for users
3. Provide a joined up experience across all channels
4. Make the service simple to use
5. Make sure everyone can use the service
6. Have a multidisciplinary team
7. Use agile ways of working
8. Iterate and improve frequently
9. Create a secure service which protects users’ privacy
10. Define what success looks like and publish performance data
11. Choose the right tools and technology
12. Make new source code open
13. Use and contribute to open standards, common components and patterns
14. Operate a reliable service

##[Development](https://www.gov.uk/service-manual/technology)
Software Development Process, Frontend build, Testing, Managing a live service, Integrating (APIs), Hosting, Protect user information

**Decisions about technology**

- avoid lock-in
- use [gov standard components](https://www.gov.uk/service-toolkit#components) - Verify (identity assuarnce), Notify (send messages), Pay (Pay for services), Platform as a Service (gov cloud hosting,), Registers (assured datasets)

###Softward development processes

- third party code
    - dependency management package to keep up to date
    (Maven, Bundler, Pip, sbt for Scala, Composer, npm)
    - manage risks, security alerts (e.g. Red Hat)
- version control - [How GDS uses GitHub](https://technology.blog.gov.uk/2014/01/27/how-we-use-github/)
- software release cycle - little and often
- automated testing
- continuous delivery
- zero-downtime deployment (blue-green deployment / image-based deployments)
- pipeline
- configuration that varies between environments
- smoke tests after deployment

